static char help[] = "Solves a linear system imported form ginzburgh-landau tao example";

#include "petscsles.h"

#undef __FUNCT__
#define __FUNCT__ "main"
int main(int argc, char *argv[]) 
{
  Vec x, b;
  Mat A;
  SLES sles;
  PC pc;
  KSP ksp;
  int its;
  int ierr;
  char matfilename[64], vecfilename[64];
  PetscTruth flg;

  PetscViewer mat_file;
  PetscViewer vec_file;

  PetscInitialize(&argc, &argv, (char *)0, help);

  ierr = PetscOptionsString("-matfile","matrix file name", PETSC_NULL,PETSC_NULL,matfilename,63,&flg);
  if (!flg) {
    SETERRQ(1,"You must indicate matlab matrix file with -matfile option");
  }
  ierr = PetscOptionsString("-vecfile","vector file name", PETSC_NULL,PETSC_NULL,vecfilename,63,&flg);
  if (!flg) {
    SETERRQ(1,"You must indicate matlab vector file with -vecfile option");
  }
  

  ierr = PetscViewerBinaryOpen(PETSC_COMM_WORLD, matfilename, PETSC_BINARY_RDONLY,&mat_file); CHKERRQ(ierr);
  ierr = PetscViewerBinaryOpen(PETSC_COMM_WORLD, vecfilename, PETSC_BINARY_RDONLY,&vec_file); CHKERRQ(ierr);


  ierr = MatLoad(mat_file, MATMPIAIJ, &A); CHKERRQ(ierr);
  ierr = VecLoad(vec_file, &b); CHKERRQ(ierr);

  ierr = PetscViewerDestroy(mat_file); CHKERRQ(ierr);
  ierr = PetscViewerDestroy(vec_file); CHKERRQ(ierr);

  ierr = VecDuplicate(b, &x); CHKERRQ(ierr);

  ierr = SLESCreate(PETSC_COMM_WORLD, &sles); CHKERRQ(ierr);

  ierr = SLESSetOperators(sles, A, A, DIFFERENT_NONZERO_PATTERN); CHKERRQ(ierr);

  ierr = SLESGetKSP(sles, &ksp); CHKERRQ(ierr);
  ierr = SLESGetPC(sles, &pc); CHKERRQ(ierr);
  ierr = PCSetType(pc, PCBJACOBI); CHKERRQ(ierr);

  ierr = SLESSetFromOptions(sles); CHKERRQ(ierr);

  ierr = SLESSolve(sles, b, x, &its); CHKERRQ(ierr);

  ierr = SLESView(sles, PETSC_VIEWER_STDOUT_WORLD); CHKERRQ(ierr);
  

  ierr = VecDestroy(x); CHKERRQ(ierr);
  ierr = VecDestroy(b); CHKERRQ(ierr);
  ierr = MatDestroy(A); CHKERRQ(ierr);

  ierr = PetscFinalize(); CHKERRQ(ierr);

  return 0;
}
